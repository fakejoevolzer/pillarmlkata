\documentclass[a4paper,10pt]{article}

\usepackage[USenglish]{babel} %francais, polish, spanish, ...
\usepackage[T1]{fontenc}
\usepackage[ansinew]{inputenc}
\usepackage{lmodern} %Type1-font for non-english texts and characters
\usepackage{graphicx} %%For loading graphic files
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}

\begin{document}
\pagestyle{empty} %No headings for the first pages.
\title{Machine Learning Kata}
\author{Joe Volzer}
\maketitle
\pagestyle{plain} %Now display headings: headings / fancy / ...

\section{Preliminary Work}
Before building the machine learning models I would consult with this client on database design and data cleanliness best practices. There are a number of issues that should be sorted out.

\subsection{Helping the Client with Data Best Practices}

The first thing that I noticed is the inconsistency of the description feature. I expected a one-to-one correspondence between stock code and description, but this is not the case. While I can not be entirely certain, it seems as though the customer is using the description field as a place for additional notes. There are instances where a product is described as ``sold as set on dotcom'' or ``printing smudges/thrown away,'' among other things. These non-description descriptions almost always coincide with a negative quantity, leading me to believe that this is how they account for returns.\\

With these pre-existing notations in mind, I would suggest the database design laid out in tables \ref{tab:dbproduct}, \ref{tab:dbinvoice}, \ref{tab:dbreturn}, and \ref{tab:dbcustomer}. This design does not take into account postage or shipping information. It merely accounts for the available information that will be used to construct machine learning models. If my conjecture that negative quantities holds true, then there is already a consistent system for denoting returns. The database is designed around the current system rather than devising a new notation convention. This should make the transition to the new schema feel more natural for the customer. It should also make implementing on-line learning for the machine learning models easier. \\

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|p{8cm}|}
\hline
\textbf{Column Name} & \textbf{Description} \\
\hline 
StockCode & A unique 5-digit number identifyingthe product. Primary Key for this table.\smallskip\\
Description & A brief description of the product.\smallskip\\
Quantity & Current Quantity in Stock. This number is ALWAYS positive. \smallskip\\
UnitPrice & Cost of one item. \smallskip\\ 
ProductCluster & A number identifying which product cluster this particular product belongs to.\smallskip\\
\hline
\end{tabular}\caption{Suggested Form of Product Table}
\label{tab:dbproduct}
\end{center}
\end{table}

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|p{8cm}|}
\hline
\textbf{Column Name} & \textbf{Description} \\
\hline
InvoiceNumber & A unique number identifying a customers order\smallskip\\ 
CustomerID & Customer Associated with the invoice\smallskip\\
PurchaseOrder & A table whose rows contain three numbers: StockCode of item purchased, quanity purchased, and discount applied.\smallskip\\
Notes & A field where additional notes can be made about the transaction\\
\hline
\end{tabular}\caption{Suggested Form of Invoice Table}
\label{tab:dbinvoice}
\end{center}
\end{table}

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|p{8cm}|}
\hline
\textbf{Column Name} & \textbf{Description} \\
\hline
InvoiceCode & A unique 5-digit number identifying the invoice. Primary Key for this table.\smallskip\\
ReturnedItems & Similar to the PurchaseOrder row in table \ref{tab:dbinvoice}. The main difference is that the quantities can only be negative.\\
Notes & A field where additional notes can be made about the transaction\\
\hline
\end{tabular}\caption{Suggested Form of Return Table}
\label{tab:dbreturn}
\end{center}
\end{table}

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|p{8cm}|}
\hline
\textbf{Column Name} & \textbf{Description} \\
\hline
CustomerID & A unique number identifying the Customer. Primary Key for this table.\smallskip\\
Country & Location of the customer. \smallskip\\
CustomerCluster & Similar to the ProductCluster row in table \ref{tab:dbproduct}.\\
\hline
\end{tabular}\caption{Suggested Form of Customer Table}
\label{tab:dbcustomer}
\end{center}
\end{table}


\subsection{Understanding the Data Set}

In this data set we are given eight features: InvoiceNo, StockCode, Description, Quantity, InvoiceDate, UnitPrice, CustomerID, and Country. 

The InvoiceNo has a many-to-one correspondence with the CustomerID. This will be useful later on when trying to build customer purchasing profiles. It also suggests a natural structure for a database.

StockCode typically has a one-to-one correspondance with Description. This is not always the case as the description feature will sometimes be used to describe the type of transcation, rather than the item itself. It is not uncommon to There are some instances where items with related descriptions have sequential stock codes. While it is tempting to use this when building machine learning models, this relationship will not always hold true. Consider the stock codes in table \ref{tab:codes}.

\begin{table}[h]
\begin{center}
\begin{tabular}{|r|l|}
\hline
\textbf{StockCode} & \textbf{Description} \\
\hline
22915 & ASSORTED BOTTLE TOP  MAGNETS\\
22916 & HERB MARKER THYME \\
22917 & HERB MARKER ROSEMARY \\
22918 & HERB MARKER PARSLEY \\
22919 & HERB MARKER MINT \\
22920 & HERB MARKER BASIL \\
22921 & HERB MARKER CHIVES \\
22922 & FRIDGE MAGNETS US DINER ASSORTED \\
\hline
\end{tabular}\caption{Sequential Stock Code and Descriptions}
\label{tab:codes}
\end{center}
\end{table}

Suppose the customer starts selling and herb marker for sage. The stock 22915 and 22922 are already taken by non herb marker products. This means that we cannot directly use the StockCode for any sort of clustering algorithm. Instead, I propose using a word2vec-style embedding with the corpus being all of the product descriptions. This style of embedding should allow for a more detailed understanding of product demand. In additon to considering the demand of handbags or herb markers, the word2vec embedding should allow for analysis of products based on color other descriptors present in the corpus. A word of caution, this could lead to some problems, such as clustering based on the word ``assorted'' or other spurious descriptors. This sort of feature would probably not be present in a first pass. \\

Quantity is a straightforward feature. It may benefit from binning. The exact quantity of products ordered might not matter, only that the quantity ordered falls within a certain range. Exploratory analysis would need to be performed to see if this is the case.\\

InvoiceDate is tricky. There is a temptation to treat demand forecasting as a classical time series problem, and to an extent, it is. Autoregression will likely fail on a problem like this. The purchasing patterns for particular products are sparse. Given this observed structure, I would suggest Factorization Machines (FM) as a method for demand forecasting. The model can be evaluated in linear time and FM perform well on sparse data sets. The linear-time computational complexity also means that on-line learning is not cost prohibitive. Since we only have one year of data, I would not focus on using the month of purchase as a feature. There simply is not enough data to extract patterns at the monthly level. ``Days since the first of the month'' or ``days until the end of the month'' could be useful features for demand forecasting. Since the client's customers are wholesalers and retailers, I would consider a boolean feature indicating whether or not today is considered a business day. \\

The UnitPrice feature will may play a role in demand forecasting, but I expect its main use to be in product clustering. This will be discussed in detail in a later section.

CustomerID and Country are straightforward features. 

\section{Building the Machine Learning Model}
Let $D^{(j)}_{i}(t)$ denote the demand for product $i$ from customer $j$ at time $t$. Using this notation, we define the total demand for product $i$ at time $t$ as 
\begin{equation}
P_i(t) := \sum_j D^{(j)}_{i}(t).
\end{equation}
Similarly, we can define total demand from Customer $j$ at time $t$ as 
\begin{equation}
C_j(t) := \left[D^{(j)}_{1}(t) \, D^{(j)}_{2}(t) \, \dots \, D^{(j)}_{m}(t) \right].
\end{equation}
Note that the output of the model $C_j(t)$ is a vector, where as the output of the model $P_i(t)$ is a number. Using our notation, we can define total demand at time $t$ as
\begin{align*}
T(t) &= \sum_j C_j(t)\\
&=  \sum_j \left[D^{(j)}_{1}(t) \, D^{(j)}_{2}(t) \, \dots \, D^{(j)}_{m}(t) \right]\\
&= \left[\sum_j D^{(j)}_{1}(t) \, \sum_j D^{(j)}_{2}(t) \, \dots \, \sum_j D^{(j)}_{m}(t) \right]\\
&= \left[P_{1}(t) \, P_{2}(t) \, \dots \, P_{m}(t) \right].
\end{align*}

We would like to build a system that is useful for new customers and new products. The idea is create a set of clusters for customers. These clusters would group customers based on some set of criteria. A new customer does not have an extensive purchasing history so we cannot build a a useful demand forecasting model using the same methods. What can do is use a Bayesian mean.\\

Let $\mathcal{C}$ denote the set of ``prototypical'' customers. Let $\mathcal{C}_{\ell}$ be the $\ell^{th}$ prototype customer. Let $C_{\mathcal{C}_{\ell}}(t)$ denote the demand of the $\ell^{th}$ prototype customer at time $t$. This model would be built using aggregated purchasing data from all customers that belong to the $\ell^{th}$ cluster. Without loss of generality, we can assume that the $j^{th}$ customer is best represented by $\mathcal{C}_{\ell}$. Suppose we only have data for 5 transactions for the $j^{th}$ customer, but we believe that 250 transactions are needed before we can fully trust our model. Rather than forecasting using $C_j(t)$, we would use 
\begin{equation}
\widehat{C}_j(t) = \frac{5}{250 + 5} C_j(t) + \frac{250}{250+5} C_{\mathcal{C}_\ell}(t)
\end{equation}
as our predictive model. As the customer makes more transactions, say 30 more, we would update the Bayesian mean to 
\begin{equation}
\widehat{C}_j(t) = \frac{35}{250 + 35} C_j(t) + \frac{250}{250+35} C_{\mathcal{C}_\ell}(t).
\end{equation}

This section is left un-finished due to the time box.

\section{If I Had Additional Time}
My philosophy while approaching this problem was to create a system that:
\begin{enumerate}
\item is parsimonious,
\item can be easily updated as new data is acquired, and
\item is useful for established as well as new customers.
\end{enumerate}

I would have liked to have gone into more detail about how I would approach the clustering problem. I would have also liked to have done some actual data analysis on the data set. However, given what I saw from the provided data, I thought that it was more important to help the customer get their data organized. I think this will save a lot trouble in the long run.\\

Building a model from scratch is expensive, especially when you have as many clients and products as this company has. Insisting on using models that are capable of online learning ensures that we are not profiligate with our computation time. Without online learning we merely update our existing models in light of new data, rather than recreating them. I like the idea of using the Bayesian mean for modeling demand. This technique is well established and intuitive; as we gain more data specific to a customer, we should gain trust in the demand forecasting model for the customer.\\

I have written documents like this when I have collaborated with development teams in the past. Those documents typically included pseudocode, much more detailed equations, and several test cases. The general format would be a
\begin{itemize}
\item definition of the quantity to be calculated;
\item an algorithm that does the calculation; either an
\begin{itemize}
\item open source solution that I have tested, or
\item a custom routine I have written if an open source solution is unavailable;
\end{itemize}
\item a test case to know when the calculation have been performed correctly.
\end{itemize}

The documents I have written in the past are as much for me as they are the development team. I work out the equations first by hand, I clean them up and adopt consistent notation, type them up and proofread to make sure there are no errors. Documents like this are a tremendous during the debugging process. I have spent days hunting down an errant minus signs that could easily have been located if I had a document like this.

\end{document}

